@extends('layouts.master')

@section('content')

<div class="container-fluid">
        @if(session('success'))
        <div class="alert alert-success" role="alert">
            {{ session('success') }}
        </div>
        @endif

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Tambah Product</h1>
    </div>

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <div class="card shadow">
        <div class="card-body">
            <form action="{{route('product.store')}}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="product_name">Nama Barang</label>
                    <input type="text" class="form-control" name="product_name" placeholder="Nama Barang" value="{{old('product_name')}}">
                </div>
                <div class="form-gorup">
                    <label for="category_id">Nama Brand</label>
                    <select name="category_id" class="form-control">
                        <option value="category_id">Pilih Nama Brand</option>
                        @foreach ($category as $categori)
                        <option value="{{$categori->id}}">
                            {{$categori->category_name}}
                        </option>                           
                        @endforeach
                    </select>
                    <button type="button" class="btn btn-primary btn-sm mt-1" data-toggle="modal" data-target="#staticBackdrop">
                        Tambah Brand
                    </button>
                </div>
                <br>
                <div class="form-group">
                    <label for="price">Harga</label>
                    <input type="number" class="form-control" name="price" placeholder="Harga" value="{{old('price')}}">
                </div>
                <div class="form-group">
                    <label for="stock">Stok</label>
                    <input type="number" class="form-control" name="stock" placeholder="Stok" value="{{old('stock')}}">
                </div>
                <div class="form-group">
                    <label for="image">Gambar</label>
                    <input type="text" class="form-control" name="image" placeholder="Gambar" value="{{old('image')}}">
                </div>
                <div class="form-group">
                    <label for="description">Deskripsi</label>
                    <textarea name="description" rows="10" class="d-block w-100 form-control">{{old('description')}}</textarea>
                </div>
                <button type="submit" class="btn btn-primary btn-block">
                    Simpan
                </button>
            </form>
        </div>
    </div>

</div>
<!-- Modal -->
    <div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">

    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="staticBackdropLabel">Tambah Brand</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>

        <div class="modal-body">
            <form action="{{route('category.store')}}" method="POST">
                @csrf
                <div>
                    <label for="category_name">Nama Brand</label>
                    <input type="text" class="form-control" name="category_name" placeholder="Nama Brand" value="{{old('category_name')}}">
                </div>
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </div>
        </form>
        </div>
    </div>
    </div>
@endsection
@extends('layouts.master')

@section('content')
    <div class="container-fluid">
      @if(session('success'))
      <div class="alert alert-success" role="alert">
          {{ session('success') }}
      </div>
      @endif
      <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Product</h1>
        <a href="{{route('product.create')}}" class="btn-sm btn-primary shadow-sm">
            <i class="fas fa-plus fa-sm text-white-50"></i> Tambah Produk
        </a>
      </div>
        <table class="table table-bordered table-striped table-responsive-sm">
            <thead class="thead-light">
              <tr>
                <th scope="col">No</th>
                <th scope="col">Gambar</th>
                <th scope="col">Name Brand</th>
                <th scope="col">Nama Barang</th>
                <th scope="col">Harga</th>
                <th scope="col">Stok</th>
                <th scope="col">Deskripsi</th>
                <th scope="col">Aksi</th>
              </tr>
            </thead>
            <tbody>
              @forelse ($products as $product)
              <tr>
                <td scope="col">{{$product->id}}</td>
                <td scope="col">{{$product->image}}</td>
                <td scope="col">{{$product->category->category_name}}</td>
                <td scope="col">{{$product->product_name}}</td>
                <td scope="col">{{$product->price}}</td>
                <td scope="col">{{$product->stock}}</td>
                <td scope="col">{{$product->description}}</td>
                <td>
                  <a href="{{route('product.edit', $product->id)}}" class="btn btn-info">
                      <i class="fa fa-pencil-alt"></i>
                  </a>
                  <form action="{{route('product.destroy', $product->id)}}" method="post" class="d-inline">
                      @csrf
                      @method('delete')
                      <button class="btn btn-danger">
                          <i class="fa fa-trash"></i>
                      </button>
                  </form>
                </td>
              </tr>
              @empty
              <tr>
                  <td colspan="7" class="text-center">
                      Data Kosong
                  </td>
              </tr>
              @endforelse
            </tbody>
        </table>
    </div>
        
@endsection
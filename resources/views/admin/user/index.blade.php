@extends('layouts.master')

@section('content')
        <table class="table table-bordered table-striped table-responsive-sm">
            <thead class="thead-light">
              <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Username</th>
                <th>Role</th>
                <th>Email</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>

            @forelse ($penduduk as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->nama}}</td>
                        <td>{{$value->nik}}</td>
                        <td>{{$value->tgl_lahir}}</td>
                        <td>{{$value->umur}}</td>
                        <td>{{$value->alamat}}</td>
                        <td>
                            <a href="/penduduk/{{$value->id}}" class="btn btn-info btn-sm">Show</a>
                            <a href="/penduduk/{{$value->id}}/edit" class="btn btn-primary btn-sm">Edit</a>
                            <form action="/penduduk/{{$value->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1 btn-sm" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td class="text-center"colspan="7">No data</td>
                    </tr>  
                @endforelse
                
            </tbody>
        </table>
@endsection
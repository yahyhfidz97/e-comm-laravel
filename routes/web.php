<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::prefix('admin')
    ->namespace('Admin')
    ->group(function(){
        Route::get('/', 'DashboardController@index')
        ->name('dashboard');
        
        Route::Resource('order', 'OrderController');
        Route::Resource('product', 'ProductController');
        Route::Resource('category', 'CategoryController');
        Route::Resource('user', 'UserController');
        
    });

//for example
    // Route::post('/siswa/create','SiswaController@create');
    // Route::get('/siswa/{id}/edit','SiswaController@edit');
    // Route::post('/siswa/{id}/update','SiswaController@update');
    // Route::get('/siswa/{id}/delete','SiswaController@delete');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');